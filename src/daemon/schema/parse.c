/*
  Copyright (c) 2016, Matthias Schiffer <mschiffer@universe-factory.net>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
  \file

  ECE schema parser
*/


#include "schema.h"
#include "../../lib/blobstore/blobstore.h"

#include <libubox/ulog.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


static eced_schema_t * eced_schema_parse(const struct blob_attr *blob);


/** Parses a single string value from a "type" attribute */
static uint8_t eced_schema_parse_one_type(struct blob_attr *type) {
	if (blobmsg_type(type) != BLOBMSG_TYPE_STRING)
		return ECED_TYPE_INVALID;

	const char *str = blobmsg_get_string(type);
	if (strcmp(str, "array") == 0)
		return ECED_TYPE_ARRAY;
	if (strcmp(str, "boolean") == 0)
		return ECED_TYPE_BOOLEAN;
	if (strcmp(str, "integer") == 0)
		return ECED_TYPE_INTEGER;
	if (strcmp(str, "null") == 0)
		return ECED_TYPE_NULL;
	if (strcmp(str, "object") == 0)
		return ECED_TYPE_OBJECT;
	if (strcmp(str, "string") == 0)
		return ECED_TYPE_STRING;

	return ECED_TYPE_INVALID;
}

/** Returns the parsed value of the "type" attribute of a schema blob */
static uint8_t eced_schema_parse_type(struct blob_attr *type) {
	if (!type)
		return ECED_TYPE_ANY;

	int ret = eced_schema_parse_one_type(type);
	if (ret)
		return ret;

	if (blobmsg_type(type) != BLOBMSG_TYPE_ARRAY || !blobmsg_check_attr_list(type, BLOBMSG_TYPE_STRING))
		return ECED_TYPE_INVALID;

	struct blob_attr *cur;
	ssize_t rem;
	blobmsg_for_each_attr(cur, type, rem) {
		uint8_t v = eced_schema_parse_one_type(cur);
		if (!v || (v & ret))
			return ECED_TYPE_INVALID;

		ret |= v;
	}

	return ret;
}

int eced_schema_cmp_properties(const void *a, const void *b) {
	const eced_schema_property_t *pa = a;
	const eced_schema_property_t *pb = b;

	return strcmp(pa->key, pb->key);
}

int eced_schema_cmp_charpp(const void *a, const void *b) {
	const char *const *pa = a;
	const char *const *pb = b;

	return strcmp(*pa, *pb);
}

/**
  Parses the "properties" attribute

  schema->n_properties and schema->properties are assumed to be 0/NULL prior to this call.

  If the parse fails, schema will left be in a half-initialized state and must be freed.
*/
static bool eced_schema_parse_properties(eced_schema_t *schema, struct blob_attr *properties) {
	if (!properties)
		return true;

	if (blobmsg_type(properties) != BLOBMSG_TYPE_TABLE)
		return false;

	ssize_t len = blobmsg_check_array(properties, BLOBMSG_TYPE_UNSPEC);
	if (len < 0)
		return false;

	schema->n_properties = len;
	schema->properties = calloc(schema->n_properties, sizeof(*schema->properties));
	if (!schema->properties)
		return false;

	size_t i = 0;
	struct blob_attr *cur;
	ssize_t rem;
	blobmsg_for_each_attr(cur, properties, rem) {
		eced_schema_property_t *prop = &schema->properties[i++];
		prop->key = strdup(blobmsg_name(cur));
		prop->schema = eced_schema_parse(cur);
		if (!prop->schema)
			return false; /* No need for cleanup, eced_schema_free will work fine as n_properties is set */
	}

	qsort(schema->properties, schema->n_properties, sizeof(*schema->properties), eced_schema_cmp_properties);

	return true;
}

/**
  Parses the "required" attribute

  schema->n_required and schema->required as assumed to be 0/NULL prior to this call.

  If the parse fails, schema will left be in a half-initialized state and must be freed.
*/
static bool eced_schema_parse_required(eced_schema_t *schema, struct blob_attr *required) {
	if (!required)
		return true;

	if (blobmsg_type(required) != BLOBMSG_TYPE_ARRAY)
		return false;

	ssize_t len = blobmsg_check_array(required, BLOBMSG_TYPE_STRING);
	if (len < 0)
		return false;

	schema->n_required = len;
	schema->required = calloc(schema->n_required, sizeof(*schema->required));
	if (!schema->required)
		return false;

	size_t i = 0;
	struct blob_attr *cur;
	ssize_t rem;
	blobmsg_for_each_attr(cur, required, rem) {
		char **req = &schema->required[i++];
		*req = strdup(blobmsg_get_string(cur));
		if (!*req)
			return false;
	}

	qsort(schema->required, schema->n_required, sizeof(*schema->required), eced_schema_cmp_charpp);

	return true;
}

/**
  Parses the "additionalProperties" attribute

  schema->additionalProperties is assumed to be NULL prior to this call.
*/
static bool eced_schema_parse_additonalProperties(eced_schema_t *schema, struct blob_attr *additionalProperties) {
	if (!additionalProperties)
		return true;

	schema->additionalProperties = eced_schema_parse(additionalProperties);
	return schema->additionalProperties;
}

/**
  Parses the "items" and "additionalItems" attributes

  schema->n_tuple_items, schema->tuple_items and schema->list_items are assumed to be 0/NULL prior to this call.

  If the parse fails, schema will left be in a half-initialized state and must be freed.
*/
static bool eced_schema_parse_items(eced_schema_t *schema, struct blob_attr *items, struct blob_attr *additionalItems) {
	if (!items)
		return true;

	schema->list_items = eced_schema_parse(items);
	if (schema->list_items)
		return true;

	if (blobmsg_type(items) != BLOBMSG_TYPE_ARRAY)
		return false;

	ssize_t len = blobmsg_check_array(items, BLOBMSG_TYPE_UNSPEC);
	if (len < 0)
		return false;

	schema->n_tuple_items = len;
	schema->tuple_items = calloc(schema->n_tuple_items, sizeof(*schema->tuple_items));
	if (!schema->tuple_items)
		return false;

	size_t i = 0;
	struct blob_attr *cur;
	ssize_t rem;
	blobmsg_for_each_attr(cur, items, rem) {
		eced_schema_item_t *item = &schema->tuple_items[i++];
		*item = eced_schema_parse(cur);
		if (!*item)
			return false;
	}

	if (!additionalItems)
		return true;

	schema->list_items = eced_schema_parse(additionalItems);
	if (!schema->list_items)
		return false;

	return true;
}

/** Parses the "default" attribute */
static bool eced_schema_parse_default(eced_schema_t *schema, const struct blob_attr *def) {
	if (!def)
		return true;

	schema->default_value = ece_blobtree_parse(def);
	if (!schema->default_value)
		return false;

	if (!eced_schema_validate(schema, schema->default_value)) {
		ulog(LOG_WARNING, "invalid default value in schema\n");
		ece_blobtree_free(schema->default_value);
		schema->default_value = NULL;
		return true;
	}

	return true;
}

/** Constructs an ECE schema from a blobmsg */
static eced_schema_t * eced_schema_parse(const struct blob_attr *blob) {
	if (blobmsg_type(blob) != BLOBMSG_TYPE_TABLE || !blobmsg_check_attr_list(blob, BLOBMSG_TYPE_UNSPEC))
		return NULL;

	enum {
		ATTR_TYPE,
		ATTR_REQUIRED,
		ATTR_PROPERTIES,
		ATTR_ADDITIONAL_PROPERTIES,
		ATTR_ITEMS,
		ATTR_ADDITIONAL_ITEMS,
		ATTR_DEFAULT,
		ATTR_MAX
	};

	const struct blobmsg_policy policy[ATTR_MAX] = {
		[ATTR_TYPE] = { .name = "type" },
		[ATTR_REQUIRED] = { .name = "required" },
		[ATTR_PROPERTIES] = { .name = "properties" },
		[ATTR_ADDITIONAL_PROPERTIES] = { .name = "additionalProperties" },
		[ATTR_ITEMS] = { .name = "items" },
		[ATTR_ADDITIONAL_ITEMS] = { .name = "additionalItems" },
		[ATTR_DEFAULT] = { .name = "default" },
	};

	struct blob_attr *attr[ATTR_MAX];
	blobmsg_parse(policy, ATTR_MAX, attr, blobmsg_data(blob), blobmsg_data_len(blob));

	eced_schema_t *schema = calloc(1, sizeof(*schema));
	if (!schema)
		goto err;

	schema->type = eced_schema_parse_type(attr[ATTR_TYPE]);
	if (!schema->type)
		goto err;

	if (schema->type & ECED_TYPE_OBJECT) {
		if (!eced_schema_parse_properties(schema, attr[ATTR_PROPERTIES]))
			goto err;
		if (!eced_schema_parse_additonalProperties(schema, attr[ATTR_ADDITIONAL_PROPERTIES]))
			goto err;
		if (!eced_schema_parse_required(schema, attr[ATTR_REQUIRED]))
			goto err;

	}

	if (schema->type & ECED_TYPE_ARRAY) {
		if (!eced_schema_parse_items(schema, attr[ATTR_ITEMS], attr[ATTR_ADDITIONAL_ITEMS]))
			goto err;
	}

	if (!eced_schema_parse_default(schema, attr[ATTR_DEFAULT]))
		goto err;

	return schema;

err:
	eced_schema_free(schema);
	return NULL;
}

eced_schema_t * eced_schema_load(const char *path) {
	struct blob_attr *blob = ece_blob_load(path);
	if (!blob)
		return NULL;

	eced_schema_t *schema = eced_schema_parse(blob);

	free(blob);

	return schema;
}

eced_schema_t * eced_schema_create_empty(void) {
	eced_schema_t *schema = calloc(1, sizeof(*schema));
	if (!schema)
		return NULL;

	schema->type = ECED_TYPE_OBJECT;

	return schema;
}

void eced_schema_free(eced_schema_t *schema) {
	if (!schema)
		return;

	size_t i;

	if (schema->properties) {
		for (i = 0; i < schema->n_properties; i++) {
			eced_schema_property_t *prop = &schema->properties[i];
			free(prop->key);
			eced_schema_free(prop->schema);
		}
		free(schema->properties);
	}

	eced_schema_free(schema->additionalProperties);

	if (schema->required) {
		for (i = 0; i < schema->n_required; i++)
			free(schema->required[i]);
		free(schema->required);
	}

	if (schema->tuple_items) {
		for (i = 0; i < schema->n_tuple_items; i++)
			eced_schema_free(schema->tuple_items[i]);
		free(schema->tuple_items);
	}

	eced_schema_free(schema->list_items);

	ece_blobtree_free(schema->default_value);

	free(schema);
}

/** Prints the given number of tabs to stdout */
static void eced_indent(size_t level) {
	size_t i;
	for (i = 0; i < level; i++)
		putchar('\t');
}

/** Dumps a schema description to stdout, indented to the given level */
static void eced_schema_dump_level(const eced_schema_t *schema, size_t level) {
	eced_indent(level);
	fputs("Valid types:", stdout);
	if (schema->type & ECED_TYPE_ARRAY)
		fputs(" array", stdout);
	if (schema->type & ECED_TYPE_BOOLEAN)
		fputs(" boolean", stdout);
	if (schema->type & ECED_TYPE_INTEGER)
		fputs(" integer", stdout);
	if (schema->type & ECED_TYPE_NULL)
		fputs(" null", stdout);
	if (schema->type & ECED_TYPE_OBJECT)
		fputs(" object", stdout);
	if (schema->type & ECED_TYPE_STRING)
		fputs(" string", stdout);
	putchar('\n');

	if (schema->n_properties) {
		eced_indent(level);
		puts("Properties:");

		size_t i;
		for (i = 0; i < schema->n_properties; i++) {
			eced_schema_property_t *prop = &schema->properties[i];

			eced_indent(level+1);
			printf("Key: %s\n", prop->key);
			eced_schema_dump_level(prop->schema, level+1);
			puts("");
		}
	}

	if (schema->additionalProperties) {
		eced_indent(level);
		puts("Additional properties:");
		eced_schema_dump_level(schema->additionalProperties, level+1);
	}

	if (schema->n_required) {
		eced_indent(level);
		puts("Required:");

		size_t i;
		for (i = 0; i < schema->n_required; i++) {
			eced_indent(level+1);
			printf("%s\n", schema->required[i]);
		}
	}

	if (schema->n_tuple_items) {
		eced_indent(level);
		puts("Tuple items:");

		size_t i;
		for (i = 0; i < schema->n_tuple_items; i++) {
			eced_schema_dump_level(schema->tuple_items[i], level+1);
			puts("");
		}
	}

	if (schema->list_items) {
		eced_indent(level);
		puts("List items:");
		eced_schema_dump_level(schema->list_items, level+1);
	}
}

void eced_schema_dump(const eced_schema_t *schema) {
	if (!schema) {
		puts("NULL");
		return;
	}

	eced_schema_dump_level(schema, 0);
}
