/*
  Copyright (c) 2016, Matthias Schiffer <mschiffer@universe-factory.net>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
  \file

  ECE schema parser and validator - public definitions
*/


#pragma once


#include "types.h"

#include "../lib/blobtree/blobtree.h"


typedef struct eced_schema eced_schema_t;


/** Loads an ECE schema file */
eced_schema_t * eced_schema_load(const char *path);

/** Creates a new schema without any entries */
eced_schema_t * eced_schema_create_empty(void);

/** Releases all memory related to a ECE schema */
void eced_schema_free(eced_schema_t *schema);

/** DEBUG: Dump a schema to stdout */
void eced_schema_dump(const eced_schema_t *schema);

/** Validates a configuration against an ECE schema */
bool eced_schema_validate(const eced_schema_t *schema, const ece_blobtree_t *tree);

/** Returns true if removing the given key from an object described by a schema is valid */
bool eced_schema_validate_delete_property(const eced_schema_t *schema, const char *key);

/** Returns true if removing the item with the given index from an array is valid */
bool eced_schema_validate_delete_item(const eced_schema_t *schema, const ece_blobtree_t *tree, size_t index);

/** Returns true if inserting the items at the given index is valid */
bool eced_schema_validate_insert_items(const eced_schema_t *schema, const ece_blobtree_t *tree, size_t index, const ece_blobtree_t *items);

/** Returns true if prepending/appending the given the items is valid */
bool eced_schema_validate_extend_items(const eced_schema_t *schema, const ece_blobtree_t *tree, const ece_blobtree_t *prepend, const ece_blobtree_t *append);

/** Type of handlers for invalid entries encountered by eced_schema_validify_at */
typedef void (*eced_schema_validify_cb_t)(const char *path, ece_blobtree_t *value, void *arg);

/**
  Transforms the given blobtree into a valid one

  Will return true if the transformation was successful, false otherwise.
*/
bool eced_schema_validify_at(const char *path, const eced_schema_t *schema, ece_blobtree_t *tree, eced_schema_validify_cb_t cb, void *arg);


/**
  Merges two schemas

  Default values from the second schema have precedence.
*/
eced_schema_t * eced_schema_merge(const eced_schema_t *schema1, const eced_schema_t *schema2);


/** Returns the subschema for given object schema and property name */
const eced_schema_t * eced_schema_lookup_property(const eced_schema_t *schema, const char *key);

/** Returns the subschema for given array schema and item index */
const eced_schema_t * eced_schema_lookup_item(const eced_schema_t *schema, size_t index);

/** Generates a blobtree of the default value for a given schema, rooted at the given path */
ece_blobtree_t * eced_schema_get_default_at(const char *path, const eced_schema_t *schema);

/** Returns true if the schema contains an explicit default value */
bool eced_schema_has_explicit_default(const eced_schema_t *schema);
