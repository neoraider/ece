/*
  Copyright (c) 2016, Matthias Schiffer <mschiffer@universe-factory.net>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
  \file

  Functions to load, access and modify the configuration - public definitions
*/


#pragma once


#include "schema.h"

#include "../lib/blobtree/blobtree.h"


/** A pointer to a configuration object and its schema */
typedef struct eced_config_pointer {
	const eced_schema_t *schema;        /**< The schema; will be NULL when this pointer is invalid */
	ece_blobtree_t *object;             /**< The configuration object */
	const eced_schema_t *parent_schema; /**< The schema of the parent */
	ece_blobtree_t *parent;             /**< The configuration object's parent (or NULL if the object is the configuration root) */
	const char *last;                   /**< The last component of the path */
} eced_config_pointer_t;


/** The invalid eced_config_pointer_t */
#define ECED_CONFIG_NULL ((eced_config_pointer_t){NULL, NULL, NULL, NULL, NULL})


/** Initializes the config schema */
bool eced_config_init(const char *schema_directory, const char *diff_filename);

/** Reloads the schemas and reinitializes the configuration */
bool eced_config_reload(void);

/** Stores the current config diff into the loaded diff file if there are any changes */
bool eced_config_commit(void);

/** Schedules a commit of the current config diff */
void eced_config_schedule_commit(void);

/** Frees the loaded schema and default data */
void eced_config_free(void);

/** Returns a pointer to the configuration root */
eced_config_pointer_t eced_config_root(void);


enum {
	ECED_LOOKUP_F_ALLOW_MISSING = (1 << 0),
	ECED_LOOKUP_F_NO_ARRAYS     = (1 << 1),
};

/**
  Looks up a configuration object with its schema in the config tree

  If the ECED_LOOKUP_F_NO_ARRAYS flag is set, rest will contain the rest of the path after the call.

  The last component of the returned path will point to memory in the given path argument.

  rest is optional.
*/
eced_config_pointer_t eced_config_lookup_path(eced_config_pointer_t root, const char *path, const char **rest, unsigned flags);

/** Inserts the given value or deletes whatever is at the given path */
bool eced_config_modify(
	const char *path, const struct blob_attr *value, const struct blob_attr *insert,
	const struct blob_attr *prepend, const struct blob_attr *append, bool reset,
	bool validify, bool store_invalid);

/**
  Removes all unapplicable diff entries that are loaded at the moment

  Will return true if any diffs have been removed, false otherwise.
*/
bool eced_config_clear_invalid(void);
