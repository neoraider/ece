/*
  Copyright (c) 2016, Matthias Schiffer <mschiffer@universe-factory.net>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
  \file

  ece CLI utility
*/


#include "../../lib/libece/libece.h"

#include <libubox/blobmsg_json.h>
#include <json-c/json.h>

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/** Prints a short usage message */
static int usage(void) {
	fputs("Usage:\n", stderr);
	fputs("\tece get [<path>]\n", stderr);
	fputs("\tece set <path> <value>\n", stderr);
	fputs("\tece insert <path> <value> [<value> ...]\n", stderr);
	fputs("\tece prepend <path> <value> [<value> ...]\n", stderr);
	fputs("\tece append <path> <value> [<value> ...]\n", stderr);
	fputs("\tece delete <path>\n", stderr);
	fputs("\tece reset <path>\n", stderr);

	return 1;
}

/** Converts a JSON string to a blobmsg and adds it to a blob_buf */
static bool ece_add_json_string(struct blob_buf *b, const char *value) {
	errno = 0;
	struct json_object *obj = json_tokener_parse(value);
	if (errno)
		return false;

	bool ret = blobmsg_add_json_element(b, NULL, obj);
	json_object_put(obj);

	return ret;
}

/** Converts an JSON strings to blobmsgs and adds them to a blob_buf */
static bool ece_add_json_strings(struct blob_buf *b, size_t n, char **values) {
	void *c = blobmsg_open_array(b, NULL);
	if (!c)
		return false;

	size_t i;
	for (i = 0; i < n; i++) {
		if (!ece_add_json_string(b, values[i]))
			return false;
	}

	blobmsg_close_array(b, c);

	return true;
}

/** Retrieves and prints the config value at the given path */
static int ece_get(struct ubus_context *ctx, const char *path) {
	int err;
	struct blob_attr *msg = libece_get(ctx, path, &err);
	if (!msg) {
		fprintf(stderr, "Command failed: %s\n", ubus_strerror(err));
		return 1;
	}

	const struct blobmsg_policy policy = { .name = "value", .type = BLOBMSG_TYPE_UNSPEC };
	struct blob_attr *attr;
	blobmsg_parse(&policy, 1, &attr, blob_data(msg), blob_len(msg));

	char *str = NULL;
	if (attr)
		str = blobmsg_format_json_value_indent(attr, 0);

	free(msg);

	if (!str)
		return 1;

	puts(str);
	free(str);

	return 0;
}

/**
  Sets the config value at the given path

  The value is interpreted as JSON, so strings need to be enclosed in "".
*/
static int ece_set(struct ubus_context *ctx, const char *path, const char *value) {
	struct blob_buf b = {};
	blob_buf_init(&b, 0);
	if (!ece_add_json_string(&b, value)) {
		fprintf(stderr, "Failed to parse value\n");
		return 1;
	}

	int err = libece_set(ctx, path, blob_data(b.head));

	blob_buf_free(&b);

	if (err) {
		fprintf(stderr, "Command failed: %s\n", ubus_strerror(err));
		return 1;
	}

	return 0;
}


/** Type for the libece calls for modifying configuration */
typedef int (*ece_modify_func)(struct ubus_context *ctx, const char *path, const struct blob_attr *value);

/**
  Modifies the config value at the given path using any modification function

  The value is interpreted as JSON, so strings need to be enclosed in "".
*/
static int ece_modify(struct ubus_context *ctx, const char *path, size_t n, char **values, ece_modify_func f) {
	struct blob_buf b = {};
	blob_buf_init(&b, 0);
	if (!ece_add_json_strings(&b, n, values)) {
		fprintf(stderr, "Failed to parse value\n");
		return 1;
	}

	int err = f(ctx, path, blob_data(b.head));

	blob_buf_free(&b);

	if (err) {
		fprintf(stderr, "Command failed: %s\n", ubus_strerror(err));
		return 1;
	}

	return 0;
}

/** Deletes the value at the given path */
static int ece_delete(struct ubus_context *ctx, const char *path) {
	int err = libece_delete(ctx, path);
	if (err) {
		fprintf(stderr, "Command failed: %s\n", ubus_strerror(err));
		return 1;
	}

	return 0;
}

/** Resets the value at the given path to its default */
static int ece_reset(struct ubus_context *ctx, const char *path) {
	int err = libece_reset(ctx, path);
	if (err) {
		fprintf(stderr, "Command failed: %s\n", ubus_strerror(err));
		return 1;
	}

	return 0;
}

/** The main function of the ece CLI utility */
int main(int argc, char **argv) {
	int ret;

	if (argc < 2)
		return usage();

	struct ubus_context *ctx = ubus_connect(NULL);
	if (!ctx) {
		fprintf(stderr, "Failed to connect to ubus\n");
		return 1;
	}

	if (strcmp(argv[1], "get") == 0) {
		if (argc != 2 && argc != 3)
			return usage();

		ret = ece_get(ctx, argc == 3 ? argv[2] : "");
	}
	else if (strcmp(argv[1], "set") == 0) {
		if (argc != 4)
			return usage();

		ret = ece_set(ctx, argv[2], argv[3]);
	}
	else if (strcmp(argv[1], "insert") == 0) {
		if (argc < 4)
			return usage();

		ret = ece_modify(ctx, argv[2], argc-3, argv+3, libece_insert_list);
	}
	else if (strcmp(argv[1], "prepend") == 0) {
		if (argc < 4)
			return usage();

		ret = ece_modify(ctx, argv[2], argc-3, argv+3, libece_prepend_list);
	}
	else if (strcmp(argv[1], "append") == 0) {
		if (argc < 4)
			return usage();

		ret = ece_modify(ctx, argv[2], argc-3, argv+3, libece_append_list);
	}
	else if (strcmp(argv[1], "delete") == 0) {
		if (argc != 3)
			return usage();

		ret = ece_delete(ctx, argv[2]);
	}
	else if (strcmp(argv[1], "reset") == 0) {
		if (argc != 3)
			return usage();

		ret = ece_reset(ctx, argv[2]);
	}
	else {
		return usage();
	}

	ubus_free(ctx);

	return ret;
}
