add_executable(json2blob
  json2blob.c
  $<TARGET_OBJECTS:blobstore>
)
set_property(TARGET json2blob PROPERTY COMPILE_FLAGS "-std=c99 -Wall ${JSON_C_CFLAGS_OTHER}")
set_property(TARGET json2blob PROPERTY LINK_FLAGS "${JSON_C_LDFLAGS_OTHER}")
set_property(TARGET json2blob PROPERTY INCLUDE_DIRECTORIES ${JSON_C_INCLUDE_DIR} ${UBOX_INCLUDE_DIR})
target_link_libraries(json2blob ${JSON_C_LIBRARIES} ${UBOX_LIBRARY} ${UBOX_BLOBMSG_JSON_LIBRARY})
