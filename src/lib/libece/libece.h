/*
  Copyright (c) 2016, Matthias Schiffer <mschiffer@universe-factory.net>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
  \file

  ECE access library
*/


#ifndef _LIBECE_H_
#define _LIBECE_H_

#include <libubus.h>


/**
  Retrieves the configuration value at the given path

  The returned blob_attr must be freed by the caller.

  The error argument is optional. On errors NULL is returned, and error is set to
  the ubus error if given.
*/
struct blob_attr * libece_get(struct ubus_context *ctx, const char *path, int *error);


/** Sets or replaces a configuration value at a given path */
int libece_set(struct ubus_context *ctx, const char *path, const struct blob_attr *value);


/** Resets the value at the given path to its default */
int libece_reset(struct ubus_context *ctx, const char *path);

/**
  Inserts a configuration value at a given path

  The path must reference an array element.
*/
int libece_insert(struct ubus_context *ctx, const char *path, const struct blob_attr *value);

/**
  Inserts a list of configuration values at a given path

  The path must reference an array element, value must have type BLOBMSG_TYPE_ARRAY.
*/
int libece_insert_list(struct ubus_context *ctx, const char *path, const struct blob_attr *value_list);


/**
  Prepends and/or appends configuration values at a given path

  The path must reference an array.

  At least one of prepend and append must be given.
*/
int libece_extend(struct ubus_context *ctx, const char *path, const struct blob_attr *prepend, const struct blob_attr *append);

/**
  Prepends a configuration value at a given path

  The path must reference an array.
*/
static inline int libece_prepend(struct ubus_context *ctx, const char *path, const struct blob_attr *value) {
	return libece_extend(ctx, path, value, NULL);
}

/**
  Appends a configuration value at a given path

  The path must reference an array.
*/
static inline int libece_append(struct ubus_context *ctx, const char *path, const struct blob_attr *value) {
	return libece_extend(ctx, path, NULL, value);
}


/**
  Prepends and/or appends lists of configuration values at a given path

  The path must reference an array.

  The values must have type BLOBMSG_TYPE_ARRAY or be NULL. At least one of prepend_list and append_list must be given.
*/
int libece_extend_list(struct ubus_context *ctx, const char *path, const struct blob_attr *prepend_list, const struct blob_attr *append_list);

/**
  Prepends a lists of configuration values at a given path

  The path must reference an array, values must have type BLOBMSG_TYPE_ARRAY.
*/
static inline int libece_prepend_list(struct ubus_context *ctx, const char *path, const struct blob_attr *value_list) {
	return libece_extend_list(ctx, path, value_list, NULL);
}

/**
  Appends a lists of configuration values at a given path

  The path must reference an array, values must have type BLOBMSG_TYPE_ARRAY.
*/
static inline int libece_append_list(struct ubus_context *ctx, const char *path, const struct blob_attr *value_list) {
	return libece_extend_list(ctx, path, NULL, value_list);
}


/** Deletes a configuration value at a given path */
int libece_delete(struct ubus_context *ctx, const char *path);

#endif /* _LIBECE_H_ */
