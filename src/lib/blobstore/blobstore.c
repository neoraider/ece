/*
  Copyright (c) 2016, Matthias Schiffer <mschiffer@universe-factory.net>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
  \file

  Data storage based on blobmsg - implementation
*/


#include "blobstore.h"

#include <errno.h>
#include <stdio.h>
#include <unistd.h>


bool ece_blob_store(const char *filename, const struct blob_attr *blob) {
	FILE *f = fopen(filename, "wb");
	if (!f)
		return false;

	if (
		fwrite(blob, blob_pad_len(blob), 1, f) != 1 ||
		fflush(f) ||
		fsync(fileno(f))
	) {
		int safe_errno = errno;
		fclose(f);
		errno = safe_errno;
		return false;
	}

	if (fclose(f))
		return false;

	return true;
}

struct blob_attr * ece_blob_load(const char *filename) {
	FILE *f = fopen(filename, "rb");
	if (!f)
		return NULL;

	struct blob_attr head;
	if (fread(&head, sizeof(head), 1, f) != 1) {
		int safe_errno = errno;
		fclose(f);
		errno = safe_errno;
		return NULL;
	}

	size_t size = blob_pad_len(&head);
	struct blob_attr *ret = malloc(size);
	if (!ret) {
		int safe_errno = errno;
		fclose(f);
		errno = safe_errno;
		return NULL;
	}

	memcpy(ret, &head, sizeof(head));
	if (fread(blob_data(ret), size - sizeof(head), 1, f) != 1) {
		int safe_errno = errno;
		fclose(f);
		errno = safe_errno;
		free(ret);
		return NULL;
	}

	if (fclose(f)) {
		free(ret);
		return NULL;
	}

	return ret;
}
