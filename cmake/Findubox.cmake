find_path(UBOX_INCLUDE_DIR libubox/uloop.h)
find_library(UBOX_LIBRARY NAMES ubox)
find_library(UBOX_BLOBMSG_JSON_LIBRARY NAMES blobmsg_json)

if(UBOX_INCLUDE_DIR AND UBOX_LIBRARY AND UBOX_BLOBMSG_JSON_LIBRARY)
  set(UBOX_FOUND TRUE)
endif(UBOX_INCLUDE_DIR AND UBOX_LIBRARY AND UBOX_BLOBMSG_JSON_LIBRARY)

if(UBOX_FOUND)
 if(NOT UBOX_FIND_QUIETLY)
   message(STATUS "Found ubox: ${UBOX_LIBRARY} ${UBOX_BLOBMSG_JSON_LIBRARY}; include path: ${UBOX_INCLUDE_DIR}")
 endif(NOT UBOX_FIND_QUIETLY)
else(UBOX_FOUND)
  if(UBOX_FIND_REQUIRED)
    message(FATAL_ERROR "Could not find ubox")
  endif(UBOX_FIND_REQUIRED)
endif(UBOX_FOUND)

mark_as_advanced(UBOX_INCLUDE_DIR UBOX_LIBRARY UBOX_BLOBMSG_JSON_LIBRARY)
