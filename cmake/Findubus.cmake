find_package(ubox REQUIRED)


find_path(UBUS_INCLUDE_DIR libubus.h)
find_library(UBUS_LIBRARY NAMES ubus)

if(UBUS_INCLUDE_DIR AND UBUS_LIBRARY)
  set(UBUS_FOUND TRUE)
endif(UBUS_INCLUDE_DIR AND UBUS_LIBRARY)

if(UBUS_FOUND)
 if(NOT UBUS_FIND_QUIETLY)
   message(STATUS "Found ubus: ${UBUS_LIBRARY}; include path: ${UBUS_INCLUDE_DIR}")
 endif(NOT UBUS_FIND_QUIETLY)
else(UBUS_FOUND)
  if(UBUS_FIND_REQUIRED)
    message(FATAL_ERROR "Could not find ubus")
  endif(UBUS_FIND_REQUIRED)
endif(UBUS_FOUND)

mark_as_advanced(UBUS_INCLUDE_DIR UBUS_LIBRARY)
